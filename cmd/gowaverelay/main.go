package main

import (
	"fmt"
	"time"

	"gitlab.com/pbcom_bobac/gowaverelay"
)

func main() {
	gowaverelay.Init(8)
	fmt.Println("Go Wave Relay with", gowaverelay.RelayCount, "relays")
	for i := 0; i < gowaverelay.RelayCount; i++ {
		gowaverelay.PinOn(i)
		b, _ := gowaverelay.Pin(i)
		if b {
			fmt.Printf("Pin %d is ON\n", i)
		} else {
			fmt.Printf("Pin %d is OFF\n", i)
		}

		time.Sleep(1 * time.Second)
		gowaverelay.PinOff(i)
		b, _ = gowaverelay.Pin(i)
		if b {
			fmt.Printf("Pin %d is ON\n", i)
		} else {
			fmt.Printf("Pin %d is OFF\n", i)
		}

		time.Sleep(1 * time.Second)
	}
	time.Sleep(1 * time.Second)
	gowaverelay.Done()
}
