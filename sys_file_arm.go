// +build arm

package gowaverelay

import (
	"io/ioutil"
	"log"
	"os"
)

func WriteSysClass(path, text string) error {
	f, err := os.Create(path)

	if err != nil {
		log.Print(err)
		return err
	}

	defer f.Close()

	_, err = f.WriteString(text)

	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func ReadSysClass(path string) (string, error) {
	b, err := ioutil.ReadFile(path)

	if err != nil {
		log.Print(err)
		return "", err
	}

	return string(b), nil
}
