package gowaverelay

import "fmt"

type Channels []string

// Channel to RPI pin maps
var CH = Channels{CH1, CH2, CH3, CH4, CH5, CH6, CH7, CH8}

//BCM codes as on PCB silk screen
const (
	CH1 = "5"
	CH2 = "6"
	CH3 = "13"
	CH4 = "16"
	CH5 = "19"
	CH6 = "20"
	CH7 = "21"
	CH8 = "26"
)

var (
	RelayCount int = 3 // Number of relays (3 or 8)
)

func Init(relays int) {
	RelayCount = relays
	for i := 0; i < RelayCount; i++ {
		ExportPin(i)
		SetPinDirection(i, "out")
	}
}

func Done() {
	for i := 0; i < RelayCount; i++ {
		PinOff(i)
		UnexportPin(i)
	}
}

// sets pin direction in|out
func SetPinDirection(pin int, direction string) {
	fmt.Printf("Setting pin %d direction to %s\n", pin, direction)

	// /sys/class/gpio/gpio$ch/direction
	WriteSysClass(fmt.Sprintf("/sys/class/gpio/gpio%s/direction", CH[pin]), direction)
	PinOff(pin)
}

func PinOn(pin int) {
	fmt.Printf("Setting pin %d to ON\n", pin)

	// /sys/class/gpio/gpio$ch/value
	WriteSysClass(fmt.Sprintf("/sys/class/gpio/gpio%s/value", CH[pin]), "0")
}

func PinOff(pin int) {
	fmt.Printf("Setting pin %d to OFF\n", pin)

	// /sys/class/gpio/gpio$ch/value
	WriteSysClass(fmt.Sprintf("/sys/class/gpio/gpio%s/value", CH[pin]), "1")
}

func Pin(pin int) (bool, error) {
	fmt.Printf("Getting pin %d\n", pin)
	// /sys/class/gpio/gpio$ch/value
	s, err := ReadSysClass(fmt.Sprintf("/sys/class/gpio/gpio%s/value", CH[pin]))
	if err != nil {
		fmt.Printf("Error getting pin %d\n", pin)
		return false, err
	}
	fmt.Printf("Content of %s is %s\n", fmt.Sprintf("/sys/class/gpio/gpio%s/value", CH[pin]), s)
	switch s {
	case "0\n":
		fmt.Printf("Pin %d is ON\n", pin)
		return true, nil
	case "1\n":
		fmt.Printf("Pin %d is OFF\n", pin)
		return false, nil
	default:
		fmt.Printf("Pin %d is UNKNOWN\n", pin)
		return false, fmt.Errorf("Pin %d is UNKNOWN\n", pin)
	}
}

func ExportPin(pin int) {
	fmt.Printf("Exporting pin %d as %s\n", pin, CH[pin])

	// /sys/class/gpio/export write pin name to this
	WriteSysClass("/sys/class/gpio/export", CH[pin])
}

func UnexportPin(pin int) {
	fmt.Printf("Unexporting pin %d as %s\n", pin, CH[pin])

	// /sys/class/gpio/unexport write pin name to this
	WriteSysClass("/sys/class/gpio/unexport", CH[pin])
}
