// +build amd64

package gowaverelay

import (
	"fmt"
)

func WriteSysClass(path, text string) error {
	fmt.Println("Writing to", path)
	fmt.Println("Content:", text)

	return nil
}

func ReadSysClass(path string) (string, error) {
	fmt.Println("Reading from", path)
	fmt.Println("Content:", "0")

	return "0", nil
}
